-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  jeu. 13 fév. 2020 à 18:21
-- Version du serveur :  5.7.26
-- Version de PHP :  7.2.18

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `mylist`
--

-- --------------------------------------------------------

--
-- Structure de la table `creneau`
--

DROP TABLE IF EXISTS `creneau`;
CREATE TABLE IF NOT EXISTS `creneau` (
  `idcreneau` int(11) NOT NULL AUTO_INCREMENT,
  `idplanning` int(11) NOT NULL,
  `heureDeb` int(3) DEFAULT NULL,
  `heureFin` int(3) DEFAULT NULL,
  PRIMARY KEY (`idcreneau`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `participe`
--

DROP TABLE IF EXISTS `participe`;
CREATE TABLE IF NOT EXISTS `participe` (
  `idcreneau` int(11) NOT NULL,
  `id` int(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`idcreneau`,`id`),
  KEY `idcreneau` (`idcreneau`),
  KEY `id` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `planning`
--

DROP TABLE IF EXISTS `planning`;
CREATE TABLE IF NOT EXISTS `planning` (
  `idplanning` int(11) NOT NULL AUTO_INCREMENT,
  `jour` int(1) DEFAULT NULL,
  `cycle` int(1) DEFAULT NULL,
  PRIMARY KEY (`idplanning`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `role`
--

DROP TABLE IF EXISTS `role`;
CREATE TABLE IF NOT EXISTS `role` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `label` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `role`
--

INSERT INTO `role` (`id`, `label`) VALUES
(1, 'Caissier titulaire'),
(2, 'Caissier assistant'),
(3, 'Gestionnaire de vrac titulaire'),
(4, 'Gestionnaire de vrac assistant'),
(5, 'Chargé d\'accueil titulaire'),
(6, 'Chargé d\'accueil assistant');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

DROP TABLE IF EXISTS `user`;
CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(30) NOT NULL,
  `login` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `droit` int(2) NOT NULL DEFAULT '1',
  `img` varchar(250) DEFAULT NULL,
  `token` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `nom`, `login`, `password`, `droit`, `img`, `token`) VALUES
(1, 'Cassandre', 'cassandre', 'mdpcassandre', 0, '1.jpg', ''),
(2, 'Achille', 'achille', 'mdpachille', 1, '2.jpg', ''),
(3, 'Calypso', 'calypso', 'mdpcalypso', 1, '3.jpg', ''),
(4, 'Bacchus', 'bacchus', 'mdpbacchus', 1, '4.jpg', ''),
(5, 'Diane', 'diane', 'mdpdiane', 1, '5.jpg', ''),
(6, 'Clark', 'clark', 'mdpclark', 1, '6.jpg', ''),
(7, 'Helene', 'helen', 'mdphelen', 1, '7.jpg', ''),
(8, 'Jason', 'jason', 'mdpjason', 1, '8.jpg', ''),
(9, 'Bruce', 'bruce', 'mdpbruce', 1, '9.jpg', ''),
(10, 'Pénélope', 'penelope', 'mdppenelope', 1, '10.jpg', ''),
(11, 'Ariane', 'ariane', 'mdpariane', 1, '11.jpg', ''),
(12, 'Lois', 'lois', 'mdplois', 1, '12.jpg', '');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `participe`
--
ALTER TABLE `participe`
  ADD CONSTRAINT `participe_ibfk_1` FOREIGN KEY (`idcreneau`) REFERENCES `creneau` (`idcreneau`),
  ADD CONSTRAINT `participe_ibfk_2` FOREIGN KEY (`id`) REFERENCES `user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
