<?php
require_once __DIR__ . '/vendor/autoload.php';
session_start();
use \Slim\Slim as Slim;
use Illuminate\Database\Capsule\Manager as DB;

$db = new DB();
$db->addConnection(parse_ini_file('./src/conf/conf.ini'));

$db->setAsGlobal();
$db->bootEloquent();

$app = new \Slim\Slim();

$app->get('/',function () {
    $c = new \ccd\controleurs\ControleurPrincipal();
    $c->afficherAccueil();
});


/*
 * ----------------------------------------------------------------
 *  Controleur Evenement
 * ----------------------------------------------------------------
 */

$app->get('/afficherCreneau/:id',function ($id) {
    $c = new \ccd\controleurs\ControleurCreneau();
    $c->afficherCreneau($id);
});

$app->get('/creationCreneau',function () {
    $c = new \ccd\controleurs\ControleurPlanning();
    $c->creerPlanning();
});

$app->get('/creationCreneau',function () {
    $c = new \ccd\controleurs\ControleurPlanning();
    $c->creerPlanning();
});

$app->get('/creationPlanning',function () {
    $c = new \ccd\controleurs\ControleurPlanning();
    $c->creerPlanning();
});

$app->post('/creationPlanning',function () {
    $c = new \ccd\controleurs\ControleurPlanning();
    $c->creerPlanningPost();
    header('Location: ./afficherPlannings');
    exit();
});

$app->get('/afficherPlannings', function() {
    $c = new \ccd\controleurs\ControleurPlanning();
    $c->afficherPlannings();
});

$app->get('/connexion', function() {
    $c = new \ccd\controleurs\ControleurClient();
    $c->identification();
});

$app->post('/connexion', function() {
    $c = new \ccd\controleurs\ControleurClient();
    $c->traitementIdentification();
    if (isset($_SESSION['Connexion']) == true) {
        header('Location: ./');
        exit();
    }
});

$app->get('/connexion', function() {
    $c = new \ccd\controleurs\ControleurClient();
    if (isset($_SESSION['Connexion']) == true) {
        header('Location: ./moncompte');
        exit();
    }
    else $c->identification();
});

$app->get('/deconnexion', function() {
    $c = new \ccd\controleurs\ControleurClient();
    $c->deconnexion();
});

$app->run();