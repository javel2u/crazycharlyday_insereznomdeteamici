<?php


namespace ccd\controleurs;

use ccd\modeles\Client;
use ccd\vues\VuePrincipale;
use ccd\controleurs\ControleurPrincipal;

class ControleurClient
{

    public static function setConnexion($bool){
        if (!isset( $_SESSION['Connexion']))
            $_SESSION['Connexion'] = $bool;
    }

    public  static  function isConnected(){
        if(isset($_SESSION['Connexion'])){
            return ($_SESSION['Connexion'] == true);
        }
        return false;
    }
    public  static function getIdConnexion(){
        return Client::where('tokenCli','=',$_SESSION['Compte_token'])->first()->idCli;
    }

    public function creerCompte(){
        $vueCompte = new \ccd\vues\VueClient(null);
        $vueCompte->render(1);
    }

    public function identification(){
        $vueCompte = new \ccd\vues\VueClient(null);
        $vueCompte->render(2);
    }

    public function traitementIdentification(){
        if (isset($_POST['Compte_login']) and isset($_POST['Compte_mdp'])) {
            $lg = filter_var($_POST['Compte_login'], FILTER_SANITIZE_STRING);
            $mdp = filter_var($_POST['Compte_mdp'], FILTER_SANITIZE_SPECIAL_CHARS) or preg_match('#^.{1,50}$#', $_POST['Compte_mdp']);
            if ($lg != false and $mdp != false) {
                $utilisateur = null;
                $utilisateur = \ccd\modeles\User::where('login', '=', $lg)->where('password', '=', $mdp)->first();
                if ($utilisateur != null) {
                    if (!isset($_SESSION['Compte_token'])) {
                        $_SESSION['Compte_token'] = $utilisateur->tokenCli;
                    }
                    self::setConnexion(true);
                    $vuePrincipale = new \ccd\vues\VuePrincipale(null);
                    $vuePrincipale->render('afficherAccueil');
                } else {
                    $tab = array("erreur" => "Identifiant ou mot de passe incorrect.");
                    $vueCompte = new \ccd\vues\VueClient($tab);
                    $vueCompte->render(2);
                    self::setConnexion(false);
                }
            } else {
                $tab = array("erreur" => "Identifiant ou mot de passe incorrect.");
                $vueCompte = new \ccd\vues\VueClient($tab);
                $vueCompte->render(2);
                self::setConnexion(false);
            }
        } else {
            $tab = array("erreur" => "Identifiant ou mot de passe incorrect.");
            $vueCompte = new \ccd\vues\VueClient($tab);
            $vueCompte->render(2);
            self::setConnexion(false);
        }
    }

    function deconnexion(){
        unset($_SESSION['Connexion']);
        unset($_SESSION['Compte_token']);
        $vuePrincipale = new \ccd\vues\VuePrincipale(null);
        $vuePrincipale->render('afficherAccueil');
    }



}