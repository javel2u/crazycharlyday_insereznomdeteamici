<?php
namespace ccd\controleurs;

use ccd\modeles\Client;
use ccd\modeles\Creneau;
use ccd\modeles\Participe;
use ccd\modeles\Planning;
use ccd\vues\VuePlanning as VuePlanning;


class ControleurPlanning
{

    public function indexPlanning(){
        $tab = [];
        $v = new VuePlanning($tab);
        $v->render('choixPlanning');
    }

    public function creerPlanning(){
        $tab = [];
        $v = new VuePlanning($tab);
        $v->render('CreationPlanning');
    }

    public function creerPlanningPost(){
        for($j = 1; $j < 8; $j++){
            $ev = new Planning();
            $ev->jour = $j;
            $ev->save();
            for ($i = 0; $i < 12; $i++) {
                $a = new Creneau();
                $a->idPlanning = $ev->idplanning;
                $a->heureDeb = (6 + $i);
                $a->heureFin = (7 + $i);
                $a->save();
            }
        }
    }

    public function afficherPlannings(){
        $p = Planning::get();
        $ev = null;
        foreach ($p as $plan){
            $c = Creneau::where('idplanning','=', $plan->idplanning)->get();
            $ev[]=[$plan, $c];
        }
        $v = new \ccd\vues\VuePlanning($ev);
        $v->render('afficherPlannings');
    }
}