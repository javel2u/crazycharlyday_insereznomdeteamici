<?php


namespace ccd\controleurs;

use ccd\modeles\User as User;

class ControleurPrincipal
{
    public function afficherAccueil() {
        $tab = User::get();
        $v = new \ccd\vues\VuePrincipale($tab);
        $v->render('afficherAccueil');
    }
}