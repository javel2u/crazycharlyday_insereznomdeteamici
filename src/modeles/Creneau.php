<?php


namespace ccd\modeles;


class Creneau extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'creneau';
    protected $primaryKey = 'idcreneau';
    public $timestamps = false ;
}