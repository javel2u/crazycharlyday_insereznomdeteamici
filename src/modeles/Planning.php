<?php


namespace ccd\modeles;


class Planning extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'planning';
    protected $primaryKey = 'idplanning';
    public $timestamps = false ;
}