<?php

namespace ccd\modeles;

class Role extends \Illuminate\Database\Eloquent\Model
{
    protected $table = 'role';
    protected $primaryKey = 'id';
    public $timestamps = false ;
}