<?php
namespace ccd\vues;

use ccd\modeles\Planning as Planning;

class VuePlanning
{
    public $tab;

    public function __construct($tableau) {
        $this->tab = $tableau;
    }

    public function render($selecteur) {
        switch ($selecteur){
            case 'CreationPlanning' : {
                $content = $this->CreationPlanning();
                $cd = '';
                break;
            }
            case 'choixPlanning' : {
                $content = $this->choixPlanning();
                $cd = '';
                break;
            }
            case 'creerPlanningPost' : {
                $content = $this->creationPlanningPost();
                $cd = '';
                break;
            }
            case 'afficherPlannings' : {
                $content = $this->afficherPlannings();
                $cd = '';
                break;
            }
        }
        if (isset($_SESSION['Connexion']) == false) {
            $html = <<<END
<!doctype html>
<html class="no-js" lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La Grande Epicerie</title>
    <link rel="stylesheet" href="{$cd}css/style.css">
  </head>
   <header class="menu" role="banner">
		 <div id="logo"><a href="{$cd}./"><img src="{$cd}img/logo.png"></a></div>
         <div id="menu_button">
			 <ul>
				<li><a class="bouton" href="{$cd}./">Accueil</a></li>
                <li><a class="bouton" href="{$cd}connexion">Connexion</a></li>
         	 </ul>
	   	</div>
    </header>
    <body>
  
    $content
    
    </body>
</html>
END;
        } else {
            $html = <<<END
<!doctype html>
<html class="no-js" lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La Grande Epicerie</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
   <header class="menu" role="banner">
		 <div id="logo"><a href="{$cd}./"><img src="{$cd}img/logo.png"></a></div>
         <div id="menu_button">
			 <ul>
				<li><a class="bouton" href="{$cd}./">Accueil</a></li>
                <li><a class="bouton" href="{$cd}afficherPlannings">Planning</a></li>
                <li><a class="bouton" href="{$cd}deconnexion">Deconnexion</a></li>
         	 </ul>
	   	</div>
    </header>
    <body>
  
    $content
    
    </body>
</html>
END;
        }

        echo $html;
    }

    private function choixPlanning() {
        $res = "<div id='center'>";
        $res = $res .
            "<div class='choixEvent'>
                <p>Créer un nouveau Planning !</p>
                <a class=\"bouton\" href=\"creationPlanning\">Créer</a>
                <p>Voir les Plannings !</p>
                <a class=\"bouton\" href=\"afficherPlannings\">Voir</a>
          
            </div>";
        $res = $res . "</div>";
        return $res;
    }

    private function afficherPlannings() {
        $res = "<!-- Masthead -->
<!-- End of masthead -->

<!-- End of board info bar -->

<!-- Lists container -->
<div class='padtop'>
<section class=\"lists-container\">";
        if($this->tab != null) {
            foreach ($this->tab as $planning) {
                $j = "none";
                switch ($planning[0]->jour) {
                    case 1 :
                        $j = "Lundi";
                        break;
                    case 2 :
                        $j = "Mardi";
                        break;
                    case 3 :
                        $j = "Mercredi";
                        break;
                    case 4 :
                        $j = "Jeudi";
                        break;
                    case 5 :
                        $j = "Vendredi";
                        break;
                    case 6 :
                        $j = "Samedi";
                        break;
                    case 7 :
                        $j = "Dimanche";
                        break;
                }
                $res = $res . "<div class=\"listT\">

		<h3 class=\"list-title\">{$j}</h3> 	<ul class=\"list-items\">";
                foreach ($planning[1] as $c) {
                    $res = $res . "<li> <a href='./afficherCreneau/{$c->idcreneau}'>{$c->heureDeb}:00 - {$c->heureFin}:00</a></li>";
                }
                $res = $res . "</ul></div>";
            }
        }
        return $res.
            //"<a class=\"add-list-btn btn\" href='./creationPlanning'>créer Planning</a>
        "<form action ='./creationPlanning' method=\"POST\">
        <button class=\"add-list-btn btn\" name=\"submit\">Créer un planning</button></form>

</section> </div>";
    }


    private function creationPlanning() {
        $res = "<div id='center'>";
        $res = $res .
            "<form class = creationEvenement action ='./creationPlanning' method=\"POST\">
                <fieldset> 
                    <legend>Création de l'évènement</legend>      
                    <label class='labelEvent' for='titre'>Jour [1-7] : </label><input type='text' name='jour' placeholder='jour'><br>
                </fieldset>
                <button name=\"submit\" value=\"Valider\">Créer</button>
            </form>";
        $res = $res . "</div>";
        return $res;
    }
}