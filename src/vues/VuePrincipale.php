<?php


namespace ccd\vues;

use ccd\modeles\User as User;

class VuePrincipale
{
    public $tab;

    public function __construct($tableau) {
        $this->tab = $tableau;
    }

    public function render($selecteur) {
        switch ($selecteur){
            case 'afficherAccueil' : {
                $content = $this->afficherAccueil();
                $cd = '';
                break;
            }
        }
        if (isset($_SESSION['Connexion']) == false) {
            $html = <<<END
<!doctype html>
<html class="no-js" lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La Grande Epicerie</title>
    <link rel="stylesheet" href="{$cd}css/style.css">
  </head>
   <header class="menu" role="banner">
		 <div id="logo"><a href="{$cd}./"><img src="{$cd}img/logo.png"></a></div>
         <div id="menu_button">
			 <ul>
				<li><a class="bouton" href="{$cd}./">Accueil</a></li>
                <li><a class="bouton" href="{$cd}connexion">Connexion</a></li>
         	 </ul>
	   	</div>
    </header>
    <body>
  
    $content
    
    </body>
</html>
END;
        } else {
            $html = <<<END
<!doctype html>
<html class="no-js" lang="fr" dir="ltr">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>La Grande Epicerie</title>
    <link rel="stylesheet" href="css/style.css">
  </head>
   <header class="menu" role="banner">
		 <div id="logo"><a href="{$cd}./"><img src="{$cd}img/logo.png"></a></div>
         <div id="menu_button">
			 <ul>
				<li><a class="bouton" href="{$cd}./">Accueil</a></li>
                <li><a class="bouton" href="{$cd}afficherPlannings">Planning</a></li>
                <li><a class="bouton" href="{$cd}deconnexion">Deconnexion</a></li>
         	 </ul>
	   	</div>
    </header>
    <body>
  
    $content
    
    </body>
</html>
END;
        }

        echo $html;
    }

    private function afficherAccueil()
    {
        if (isset($_SESSION['Connexion']) == false) {
            $res = "
            <div id=\"first\"> 
                <p><strong>Bienvenue sur La Grande Epicerie !</strong>
                <br>Connectez vous en appuyant ci-dessous.</p> 
                <a class=\"bouton\" href=\"Connexion\">Connexion</a>
            </div>";
        } else {
            $res = "
            <div id=\"first\"> 
                <p><strong>Bienvenue sur La Grande Epicerie !</strong>
                <br>Regardez votre planning ci dessous.</p> 
                <a class=\"bouton\" href=\"afficherPlannings\">Recherche</a>
            </div>";
        }
        $res = $res . "<div id=\"second\"><p> Site pour le CrazyCharlyDay <br><br> S3C - PERCIN SASSU JAVEL BEER </p>";
        if($this->tab!=null){
            $res = $res . "<div> <p> Bénévoles : </p>";
                foreach ($this->tab as $user) {
                    $res = $res . "<img src='./img/{$user->img}'><p> - {$user->nom}</p><br>";
                }
            $res = $res . "</div>";
        }
      return $res . "</div>";
    }
}